<?php

namespace App\Http\Controllers;

use App\Redirect;
use Illuminate\Http\Request;
use Response;

class ShortenerController extends Controller
{
    public function shorter(Request $request){

        $url = $request->input('url');
        $originalUrl = $url;
        if (in_array($url, array('', 'about:blank', 'undefined', 'http://localhost/', 'http://localhost', 'localhost'))) {
            return response()->json([
                'error' => 'Invalid URL'
            ], 400);
        }

        // If the URL is already a short URL on this domain, don’t re-shorten it
        if (strpos($url, env('SHORT_URL')) === 0) {
            return Response::json([
                'large_url' => $originalUrl,
                'short_url' => $url
            ]);
        }

        $url = addslashes($url);
        $redirect = Redirect::where('url', $url)->first();
        if($redirect){ // If there’s already a short URL for this URL
            return Response::json([
                'large_url' => $originalUrl,
                'short_url' => env('SHORT_URL')  . $redirect->slug
            ]);
        }
        else{
            $redirect = Redirect::orderBy('created_at', 'DESC')
                ->orderBy('slug', 'DESC')
                ->first();
            if($redirect) {
                $slug = $this->getNextShortURL($redirect->slug);
                $newRedirect = Redirect::insert([
                    'slug' => $slug,
                    'url' => $url
                ]);
                return Response::json([
                    'large_url' => $originalUrl,
                    'short_url' => env('SHORT_URL') . $slug
                ]);
            }
        }
    }

    public function larger(Request $request)
    {

        $slug = $request->input('url');
        $slug = parse_url($slug, PHP_URL_PATH);
        $slug = ltrim($slug, '/');
        $slug = preg_replace('/[^a-z0-9]/si', '', $slug);
        $url = env('APP_URL') . '/';
        $escapedSlug = addslashes($slug);
        $redirectResult = Redirect::where('slug', $escapedSlug)->first();
        if ($redirectResult) {
            $redirectResult->update(['hits' => $redirectResult->hits + 1]);
            return Response::json([
                'large_url' => $redirectResult->url,
                'short_url' => env('SHORT_URL')  . $redirectResult->slug
            ]);
        } else
            return response()->json([
                'error' => 'Not Found'
            ], 404);

    }













    function nextLetter(&$str) {
        $str = ('z' == $str ? 'a' : ++$str);
    }

    function getNextShortURL($s) {
        $a = str_split($s);
        $c = count($a);
        if (preg_match('/^z*$/', $s)) { // string consists entirely of `z`
            return str_repeat('a', $c + 1);
        }
        while ('z' == $a[--$c]) {
            $this->nextLetter($a[$c]);
        }
        $this->nextLetter($a[$c]);
        return implode($a);
    }
}
